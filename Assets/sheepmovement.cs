﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sheepmovement : MonoBehaviour
{
    // There will be the variables here that represent the sheep's health, speed, breeding threshold and position
    public Transform sheepPoint;
    public float health;
    public float moveSpeed;
    public float breedingthreshold;
    public float turntimer;
    public float goturn;
    public GameObject Sheep;
    public float direction;
    public Transform Spawnpoint;
    public GameObject SheepPrefab;
    public Rigidbody2D rb;
    // Update is called once per frame
    void Update()
    {
         if (turntimer > 0)
            turntimer -= Time.deltaTime;
        if (turntimer == 0)
            turntimer = 100000;
            direction = Random.Range (0, 5);
            if (direction == 1)
                rb.AddForce (new Vector2 (0, 100) * Time.deltaTime);
            else if (direction == 2)
                rb.AddForce (new Vector2 (100, 0) * Time.deltaTime);
            else if (direction == 3)
                rb.AddForce (new Vector2 (0, -100) * Time.deltaTime);
            else if (direction == 4)
                rb.AddForce (new Vector2 (-100, 0) * Time.deltaTime);
        if (health <= 0)
            Destroy(gameObject);


        //Have a loop going that does a countdown before doing the sheep's movement.
        //Have a random.range between 1-8 go with each number corresponding to a direction.
        //Have a way to detect sheep that are above the breeding threshold in terms of health.
        //Have a way for the sheep to detect boundaries
        //Have an eating function and being able to tell whether grass exists or not.
    }
    void FixedUpdate()
    {
        if (health <= 0)
            Destroy(gameObject);
    }
     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Grass")) 
        {
            health = health + 5;
        }
        if (other.gameObject.CompareTag("Sheep") && health <= 115);
        {
            Instantiate(SheepPrefab);
            health = health / 2;
        }
        if (other.gameObject.CompareTag("Wolf")) 
        {
            health = health - 10;
        }
        if (other.gameObject.CompareTag("Fence")) 
        {
            moveSpeed = 0f;
        }
        if (other.gameObject.CompareTag("Sheep")) 
        {
            Physics2D.IgnoreCollision(other.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
        }
    }
}
