﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grass : MonoBehaviour
{
    //Have a public float for grass level maybe even have it be a Random.range to add more natural variety to the grass.
    public float health;
    public bool goturn;
    public float turntimer;
    //Have a public float for regrowth rate of the grass.
    //Have a public float for the cap of how much grass can be in one square
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (health >= 1)
            gameObject.tag = "Grass";
            gameObject.SetActive(false);
        if (health <= 0)
            gameObject.tag = "Dirt";
            gameObject.SetActive(true);
        if (turntimer > 0)
            turntimer -= Time.deltaTime;
        if (turntimer == 0)
            turntimer = 10000;
            health = health + 5;
        if (health >= 1000)
            health = -1000;
        //Have whatever conditions must be met for grass to regrow
        //Have whatever formula is needed for grass regrowth.
        //Have a way for the grass to change tags from geass to dirt to grass when certain conditions are met
        //Make it have a scale of green to show much grass is on it

    }
     private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Sheep")) 
        {
            health = health - 5;
        }
    }
}
